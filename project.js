
/////////////////////////////////////////////////////////////////
///////////////	Global Variables  //////////////////////////////
///////////////////////////////////////////////////////////////
//var game = new Game(1280, 720, "Murder");
var music;
var timer;
var input;


/////////////////////////////////////////////////////////////////
///////////////	Functions //////////////////////////////////////
///////////////////////////////////////////////////////////////

function setSize(fSize) {
    var x = document.getElementsByClassName('content');
    for (var i=0; i<x.length; i++) {
        x[i].className = fSize + ' content';
    }
    sessionStorage.setItem('fontSize',fSize);
}

function loadFromSession() {
    if (sessionStorage.getItem('fontSize')===null) {
        sessionStorage.setItem('fontSize','normal');
    } else {
        setSize(sessionStorage.getItem('fontSize'));
    }
}
function clearSession() {
    //sessionStorage.clear();
}

function userMode() {
  var elem = document.getElementById("userFriendly");

      if (elem.innerHTML==="EXIT User Friendly Mode") {
        elem.innerHTML = "Enter User Friendly Mode";
        setSize('normal');

        document.getElementById("userFriendly").style.color = 'white'
        document.getElementById("userFriendly").style.backgroundColor = '#cc0000'
        document.getElementById("userFriendly").style.border = 'none'
      }
      else {
        elem.innerHTML = "EXIT User Friendly Mode";
        setSize('large');

        document.getElementById("userFriendly").style.color = 'black'
        document.getElementById("userFriendly").style.backgroundColor = 'white'
        document.getElementById("userFriendly").style.border = '2px solid #cc0000'
      }
}

function pausePlay() {
  var elem = document.getElementById("playPause");

      if (elem.innerHTML==="Pause Music") {
        elem.innerHTML = "Play Music";
        //setSize('normal');
        pauseAudio();

        document.getElementById("playPause").style.color = 'white'
        document.getElementById("playPause").style.backgroundColor = '#cc0000'
        document.getElementById("playPause").style.border = 'none'
      }
      else {
        elem.innerHTML = "Pause Music";
        //setSize('large');
        playAudio();

        document.getElementById("playPause").style.color = 'black'
        document.getElementById("playPause").style.backgroundColor = 'white'
        document.getElementById("playPause").style.border = '2px solid #cc0000'
      }
}

function displayTimer(){

  document.getElementById('timer').innerHTML =
    05 + ":" + 01;
  startTimer();

}


function startTimer() {
  var presentTime = document.getElementById('timer').innerHTML;
  var timeArray = presentTime.split(/[:]+/);
  var m = timeArray[0];
  var s = checkSecond((timeArray[1] - 1));
  if(s==59){m=m-1}

  document.getElementById('timer').innerHTML =
  m + ":" + s;
  setTimeout(startTimer, 1000);
}

function checkSecond(sec) {
  if (sec < 10 && sec >= 0) {sec = "0" + sec};
  if (sec < 0) {sec = "59"};
  return sec;
}


function storeName() {
sessionStorage.setItem('player', document.getElementById("name").value);
}

function displayName(){

  document.getElementById("name2").innerHTML = sessionStorage.getItem('player');
}


function playMusic(){

var audio = new Audio('BackgroundMusic.mp3');
audio.play();

}
function gameOver123(){
         for (i=0; i <5; i++)
         {
           if (LB[i][1] != 0)
           {
             if(timer < LB[i][1])
             {
               LB[5][0] = name;
               LB[5][0] = timer;
             }
           }
         }
   LB.sort(compareSecondColumn());

   //JSON then save in local storage
   var endJSON = JSON.stringify(LB);
   localStorage.setItem('results', myJSON);

 }


function compareSecondColumn(a, b) {
 if (a[1] === b[1]) {
     return 0;
 }
 else {
     return (a[1] < b[1]) ? -1 : 1;
 }

}

function allLetter(userInput)  {
      var letters = /^[A-Za-z ]*$/;

    //  if (userInput.value == "") {
    if (userInput.value.trim().length ==0) {
        alert("Please input a value");
        return false;
      }
      else if(userInput.value.match(letters)) {
        //playMusic();
        storeName();
        window.open('GameSlide.html');
        window.close("mainmenu.html");
        return true;
      }
      else {
        alert('Please input alphabetic characters only');
        return false;
      }
  }


  var startJSON = localStorage.getItem('results')
  var LB=new Array(5)

  for (i=0; i <5; i++){
          LB[i]=new Array(2);
          LB[i][1] = 0;
        }
